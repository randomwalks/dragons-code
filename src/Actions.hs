{-# LANGUAGE OverloadedStrings #-}

module Actions
  ( rootAction
  , notFoundAction
  , searchAction
  , regionsAction
  , regionInputAction
  , postRegionAction
  , regionIdAction
  , regionSearchAction
  , houseInputAction
  , postHouseAction
  , housesAction
  , houseIdAction
  , houseSearchAction
  , factionAction
  , characterInputAction
  , postCharacterAction
  , characterInputRelationsAction
  , postCharacterRelationsAction
  , charactersAction
  , characterAction
  , characterSearchAction
  , actorsAction
  , actorAction
  , actorSearchAction
  , episodesAction
  , episodeAction
  , episodeSearchAction
  ) where

import Web.Scotty
import Database.MySQL.Simple
import Text.Microstache
import Data.Aeson
import Data.Text.Lazy (Text, concat) -- for type annotations and concat search expressions
import Network.HTTP.Types.Status (imATeapot418, created201) -- TODO remove teapot?

import qualified Data.Text.Lazy.IO as TIO
import Control.Monad.IO.Class (liftIO) -- todo: remove?

import Data.Time.Calendar (diffDays)
import Data.Time.Clock (getCurrentTime, utctDay)

-- local modules
import Types
  ( Region     (..)
  , House      (..)
  , Character  (..)
  , Actor      (..)
  , ActorAward (..)
  , Episode    (..)
  )


connectInfo :: ConnectInfo
connectInfo = defaultConnectInfo { connectUser     = "guest"
                                 , connectPassword = "guest"
                                 , connectDatabase = "guest" }

-- Helper function
-- | Template with compiled templates base(selected)
-- | and content (which is dynamically assigned to different file templates)
currTemplate :: FilePath -> IO Template
currTemplate contentFile = do
  --             readFile :: FilePath -> IO Text
  base    <- TIO.readFile "templates/base.mustache"
  content <- TIO.readFile contentFile
  --                          compileMustacheText :: PName -> Text -> Either ParseError Template
  let Right baseTemplate    = compileMustacheText "base"    base
  let Right contentTemplate = compileMustacheText "content" content
  pure $ baseTemplate <> contentTemplate


rootAction :: ActionM ()
rootAction = do
  c  <- liftIO $ connect connectInfo
  nVassals <- liftIO $ query_ c qNVassals           :: ActionM [(Int, Text, Int)]
  counts   <- liftIO $ query_ c qCountEntries       :: ActionM [(Text, Int)]
  actorP   <- liftIO $ query_ c qActorParticipation :: ActionM [(Int, Text, Int)]

  let json = object
            [ "title"    .= ("House of the Dragon" :: Text)
            , "nVassals" .= map (jsonifyTriples "HouseId" "Name" "NVassals") nVassals

            , "counts"   .= map (\(table, count) -> object [ "Table"    .= table
                                                           , "nEntries" .= count
                                                           ])
                                counts

            , "actorParticipation" .= map (jsonifyTriples "ActorId" "Name" "Participation") actorP
            ]
  --Web.Scotty.json json
  template <- liftAndCatchIO $ currTemplate "templates/home.mustache"
  html $ renderMustache template json

-- helper function
jsonifyTriples :: Data.Aeson.Key -> Data.Aeson.Key -> Data.Aeson.Key -> (Int, Text, Int) -> Value
jsonifyTriples field1 field2 field3 (a, b, c) =
  object
    [ field1 .= a
    , field2 .= b
    , field3 .= c
    ]

qNVassals :: Query
qNVassals = " with recursive VASSAL as                         \
            \ (                                                \
            \ select H1.HouseId                                \
            \      , H1.Name                                   \
            \      , H1.OverlordedBy                           \
            \      , H2.Name as OverlordName                   \
            \ from HOUSE H1                                    \
            \ join HOUSE H2 on (H1.OverlordedBy = H2.HouseId)  \
            \                                                  \
            \ UNION DISTINCT                                   \
            \                                                  \
            \ select V.HouseId                                 \
            \      , V.Name                                    \
            \      , H1.OverlordedBy                           \
            \      , H2.Name                                   \
            \ from VASSAL V                                    \
            \ join HOUSE  H1 on (V.OverlordedBy = H1.HouseId)  \
            \ join HOUSE  H2 on (H1.OverlordedBy = H2.HouseId) \
            \ )                                                \
            \ select OverlordedBy                              \
            \      , OverlordName                              \
            \      , count(*) as NVassals                      \
            \ from VASSAL                                      \
            \ group by OverlordName, OverlordedBy              \
            \ order by NVassals desc                           \
            \        , OverlordName ;                          "

qCountEntries :: Query
qCountEntries = " select 'Episodes'   \
                \      , count(*)     \
                \ from EPISODE        \
                \ UNION               \
                \ select 'Actors'     \
                \      , count(*)     \
                \ from ACTOR          \
                \ UNION               \
                \ select 'Characters' \
                \      , count(*)     \
                \ from CHARACTER_     \
                \ UNION               \
                \ select 'Houses'     \
                \      , count(*)     \
                \ from HOUSE          \
                \ UNION               \
                \ select 'Regions'    \
                \      , count(*)     \
                \ from REGION ;       "

qActorParticipation :: Query
qActorParticipation = " select A.ActorId                             \
                      \     , A.Name                                 \
                      \     , count(*) as nParticipation             \
                      \ from ACTOR A                                 \
                      \ join ACTS_IN AI on (A.ActorId = AI.HasActor) \
                      \ group by A.ActorId                           \
                      \ order by nParticipation desc                 \
                      \        , A.Name ;                            "

notFoundAction :: ActionM ()
notFoundAction = do
  template <- liftIO $ currTemplate "templates/notFound.mustache"
  html $ renderMustache template $ object []

searchAction :: ActionM ()
searchAction = do
  template <- liftAndCatchIO $ currTemplate "templates/search.mustache"
  html $ renderMustache template (object [ "title" .= ("Search" :: Text) ])

regionsAction :: ActionM ()
regionsAction = do
  c <- liftIO $ connect connectInfo
  regions <- liftIO $ query_ c qRegions :: ActionM [Region]
  --                          [Region] -> [IO Region] ; [IO Region] -> IO [Region]
  regionsComplete <- liftIO $ sequence $ map (f c) regions
  template <- liftAndCatchIO $ currTemplate "templates/regions.mustache"
  let json = object
        [ "title"   .= ("Regions" :: Text)
        , "regions" .= regionsComplete
        ]
  --Web.Scotty.json json
  html $ renderMustache template json


qRegions :: Query
qRegions = " select R.RegionId           \
           \      , R.Name               \
           \      , R.PopulationName     \
           \      , R.BastardsSurname    \
           \      , R.RuledBy            \
           \      , H.Name               \
           \ from REGION R               \
           \ join HOUSE H                \
           \ on (R.RuledBy = H.HouseId) ;"
-- qRegions =  " select *     \
--             \ from REGION; "

f :: Connection -> Region -> IO Region
f c r = do
  places <- query c qPlaceOfNote [(regionId r)] :: IO [(Only Text)] -- single column
  pure $ r { placeOfNote = (map fromOnly places) }  -- mapping [(Only Text)] -> [Text] with deconstructor for Only

qPlaceOfNote :: Query
qPlaceOfNote = " select PlaceOfNote \
               \ from PLACE_OF_NOTE \
               \ where RegionId = ? "

regionInputAction :: ActionM ()
regionInputAction = do
  c <- liftIO $ connect connectInfo
  houses <- liftIO $ query_ c qHousesId :: ActionM [(Int, Text)]

  let json  = object
        [ "title"  .= ("REGION input" :: Text)
        , "houses" .= map (jsonifyPairs "HouseId" "Name") houses
        ]

  template <- liftAndCatchIO $ currTemplate "templates/region-input.mustache"
  html $ renderMustache template json

postRegionAction :: ActionM ()
postRegionAction = do
  ps <- params
  -- liftAndCatchIO $ sequence_ $ map TIO.putStrLn (parseMultiValued "PlaceOfNote" ps) -- devel output
  name    <- param "Name" :: ActionM Text
  popName <- param "PopulationName" :: ActionM Text
  basName <- param "BastardsSurname" :: ActionM Text
  ruledBy <- param "RuledBy" :: ActionM Text
  let places = parseMultiValued "PlaceOfNote" ps :: [Text]

  c <- liftIO $ connect connectInfo
  n <- liftIO $ execute c qRegionInsert (name, popName, basName, ruledBy)

  id <- liftIO $ insertID c
  let insertPlaces = map (\p -> execute c qPlaceInsert (id, p)) places -- :: [IO Int64]
  --       sequence :: t (m a) -> m (t a)
  liftIO $ sequence $ insertPlaces

  status created201
  regionInputAction

-- | Multivalued fields
-- | Filter pairs for the desired key then extract the value
parseMultiValued :: Text -> [Web.Scotty.Param] -> [Text]
parseMultiValued txt = map snd . filter (\(k,v) -> k == txt)

qRegionInsert :: Query
qRegionInsert = " insert into REGION ( Name            \
                \                    , PopulationName  \
                \                    , BastardsSurname \
                \                    , RuledBy)        \
                \ values (?, ?, ?, ?) ;                "

qPlaceInsert :: Query
qPlaceInsert = " insert into PLACE_OF_NOTE \
               \ values (?,?) ;            "

regionIdAction :: ActionM ()
regionIdAction = do
  id <- param "id" :: ActionM Int
  c <- liftIO $ connect connectInfo
  regions <- liftIO $ query c qRegionId [id] :: ActionM [Region]
  case regions of
    [] -> status imATeapot418 >> html "BOOM" -- no region with such id
    _  -> do
      let region = head regions

      regionComplete <- liftIO $ f c region
      houses <- liftIO $ query c qRegionHouses [id] :: ActionM [(Int, Text)]

      let json = object
            [ "title"  .= (nameRegion regionComplete)
            , "region" .= regionComplete
            , "houses" .= map (jsonifyPairs "HouseId" "Name") houses
            ]

      --Web.Scotty.json json
      template <- liftAndCatchIO $ currTemplate "templates/region.mustache"
      html $ renderMustache template json


qRegionId :: Query
qRegionId = " select R.RegionId            \
             \      , R.Name               \
             \      , R.PopulationName     \
             \      , R.BastardsSurname    \
             \      , R.RuledBy            \
             \      , H.Name               \
             \ from REGION R               \
             \ join HOUSE H                \
             \ on (R.RuledBy = H.HouseId)  \
             \ where R.RegionId = ?        ;"

qRegionHouses :: Query
qRegionHouses = " with recursive REGION_HOUSE as                \
                \ (                                             \
                \ select H.HouseId                              \
                \      , H.Name                                 \
                \ from HOUSE H                                  \
                \ join REGION R on (H.HouseId = R.RuledBy)      \
                \ where R.RegionId = ?                          \
                \                                               \
                \ UNION DISTINCT                                \
                \                                               \
                \ select H.HouseId                              \
                \      , H.Name                                 \
                \ from REGION_HOUSE RH                          \
                \ join HOUSE H on (H.OverlordedBy = RH.HouseId) \
                \ left join REGION R on (H.HouseId = R.RuledBy) \
                \ where R.RegionId is null                      \
                \ )                                             \
                \ select * from REGION_HOUSE ;                  "

regionSearchAction :: ActionM ()
regionSearchAction = do
  ps <- params
  let place = defaultHeadWrapped $ parseMultiValued "place" ps

  c  <- liftIO $ connect connectInfo
  places <- liftIO $ query c
                           qSearchPlaces
                           [place] :: ActionM [(Int, Text)]

  let json = object
        [ "title"       .= ("Places Search" :: Text)
        , "TableHeader" .= ("Place"         :: Text)
        , "endpoint"    .= ("regions"       :: Text)
        , "entities"    .= map (jsonifyPairs "Id" "Name") places
        ]

  --Web.Scotty.json json
  template <- liftAndCatchIO $ currTemplate "templates/searchResults.mustache"
  html $ renderMustache template json


qSearchPlaces :: Query
qSearchPlaces = " select R.RegionId            \
               \      , P.PlaceOfNote         \
               \ from REGION R                \
               \ natural join PLACE_OF_NOTE P \
               \ where P.PlaceOfNote like ? ; "

houseInputAction :: ActionM ()
houseInputAction = do
  c <- liftIO $ connect connectInfo
  houses <- liftIO $ query_ c qHousesId :: ActionM [(Int, Text)]
  template <- liftAndCatchIO $ currTemplate "templates/house-input.mustache"
  let json = object
        [ "title"  .= ("HOUSE input" :: Text)
        , "houses" .= map -- building [{"houseId": id, "houseName": name}, ..] for <select> tag
                        (\ (id, name) -> object [ "HouseId" .= id, "Name" .= name ])
                        houses
        ]
  --Web.Scotty.json json
  html $ renderMustache template json

qHousesId :: Query
qHousesId = " select HouseId, Name \
            \ from HOUSE ;         "

postHouseAction :: ActionM ()
postHouseAction = do
  ps <- params
  let values = p ps l
  -- liftIO $ sequence_ $ map TIO.putStrLn values -- devel output
  c <- liftIO $ connect connectInfo
  liftIO $ execute c qInsertHouse values

  houseId <- liftIO $ insertID c
  let maybeMotto = (g ps "Motto")
  case maybeMotto of
    "" -> pure ()
    _  -> liftIO $ execute c qInsertCoatOfArms (maybeMotto, houseId) >> pure ()
  houseInputAction

-- order decides order of parameters given to execute
-- which must match the parameterized query
l :: [Text]
l = ["Name", "Faction", "Castle", "Title", "CoatOfArms", "OverlordedBy"]

p :: [Web.Scotty.Param] -> [Text] -> [Text]
p params = fmap (g params)

g :: [Web.Scotty.Param] -> Text -> Text
g []    _                  = ""
g ((key, value): xs) field
  | key == field = value
  | otherwise    = g xs field

qInsertHouse :: Query
qInsertHouse = " insert into HOUSE ( Name           \
               \                   , Faction        \
               \                   , Castle         \
               \                   , Title          \
               \                   , CoatOfArms     \
               \                   , OverlordedBy ) \
               \ values (?, ?, ?, ?, ?, ?) ;        "

qInsertCoatOfArms :: Query
qInsertCoatOfArms = " update HOUSE         \
                    \ set    Motto = ?     \
                    \ where  HouseId = ? ; "

housesAction :: ActionM ()
housesAction = do
  c <- liftIO $ connect connectInfo
  houses <- liftIO $ query_ c qHouses :: ActionM [House]
  let json = object
        [ "title"  .= ("Houses" :: Text)
        , "houses" .= houses
        ]
  --Web.Scotty.json json
  template <- liftAndCatchIO $ currTemplate "templates/houses.mustache"
  html $ renderMustache template json

qHouses :: Query
qHouses = " select H1.HouseId      \
          \      , H1.Name         \
          \      , H1.Faction      \
          \      , H1.Castle       \
          \      , H1.Title        \
          \      , H1.CoatOfArms   \
          \      , H1.Motto        \
          \      , H1.OverlordedBy \
          \      , H2.Name         \
          \ from HOUSE H1          \
          \ join HOUSE H2          \
          \ on (H1.OverlordedBy = H2.HouseId) ; "

houseIdAction :: ActionM ()
houseIdAction = do
  id <- param "id" :: ActionM Int
  c  <- liftIO $ connect connectInfo
  houses <- liftIO $ query c qHouseId [id] :: ActionM [House]
  case houses of
    [] -> status imATeapot418 >> html "BOOM" -- no house with such id
    _  -> do
      let house = head houses
      [(regionId, regionName)] <- liftIO $ query c qHouseRegion [nameHouse house] :: ActionM [(Int, Text)]

      characters <- liftIO $ query c qHouseChars [id] :: ActionM [(Int, Text)]
      vassals    <- liftIO $ query c qVassals    [id] :: ActionM [(Int, Text)]

      let json  = object
            [ "title"         .= (nameHouse house)
            , "house"         .= house { houseRegionId = (Just regionId), houseRegionName = (Just regionName) }

            , "hasCharacters" .= (case length characters of 0 -> False; _ -> True )
            , "characters"    .= map (jsonifyPairs "CharId"  "Name") characters

            , "hasVassals"    .= (case length vassals of 0 -> False; _ -> True )
            , "vassals"       .= map (jsonifyPairs "HouseId" "Name") vassals

            ]
      --Web.Scotty.json json
      template <- liftAndCatchIO $ currTemplate "templates/house.mustache"
      html $ renderMustache template json

qHouseId :: Query
qHouseId = " select H1.HouseId      \
           \      , H1.Name         \
           \      , H1.Faction      \
           \      , H1.Castle       \
           \      , H1.Title        \
           \      , H1.CoatOfArms   \
           \      , H1.Motto        \
           \      , H1.OverlordedBy \
           \      , H2.Name         \
           \ from HOUSE H1          \
           \ join HOUSE H2          \
           \ on (H1.OverlordedBy = H2.HouseId) \
           \ where H1.HouseId = ? ; "


qHouseRegion :: Query
qHouseRegion =
  " with recursive cte as                       \
  \ (                                           \
  \ select H.HouseId      as houseId            \
  \      , H.Name         as houseName          \
  \      , R.RegionId     as regionId           \
  \      , R.Name         as regionName         \
  \      , H.OverlordedBy as overlordId         \
  \ from HOUSE H                                \
  \ left join REGION R on H.HouseId = R.RuledBy \
  \ where H.Name = ?                            \
  \                                             \
  \ UNION ALL                                   \
  \                                             \
  \ select H.HouseId      as houseId            \
  \      , H.Name         as houseName          \
  \      , R.RegionId     as regionId           \
  \      , R.Name         as regionName         \
  \      , H.OverlordedBy as overlordId         \
  \ from cte                                    \
  \ join HOUSE H on cte.overlordId = H.HouseId  \
  \ left join REGION R on H.HouseId = R.RuledBy \
  \ where regionName is null                    \
  \ )                                           \
  \ select regionId, regionName                 \
  \ from cte                                    \
  \ where regionName is not null;               "

qHouseChars :: Query
qHouseChars = " select C.CharId              \
              \      , C.Name                \
              \ from CHARACTER_ C            \
              \ natural join LOYAL_TO LT     \
              \ join HOUSE H using (HouseId) \
              \ where H.HouseId = ? ;        "

qVassals :: Query
qVassals = " with recursive VASSAL as                     \
           \ (                                            \
           \ select HouseId                               \
           \      , Name                                  \
           \ from HOUSE                                   \
           \ where OverlordedBy = ?                       \
           \                                              \
           \ UNION DISTINCT                               \
           \                                              \
           \ select H.HouseId                             \
           \      , H.Name                                \
           \ from VASSAL V                                \
           \ join HOUSE H on (H.OverlordedBy = V.HouseId) \
           \ )                                            \
           \ select * from VASSAL ;                       "

houseSearchAction :: ActionM ()
houseSearchAction = do
  ps <- params
  let name     = defaultHeadWrapped $ parseMultiValued "name"     ps
  let castle   = defaultHeadWrapped $ parseMultiValued "castle"   ps
  let title    = defaultHeadWrapped $ parseMultiValued "title"    ps
  let coatArms = defaultHeadWrapped $ parseMultiValued "coatarms" ps
  let motto    = defaultHeadWrapped $ parseMultiValued "motto"    ps

  c  <- liftIO $ connect connectInfo
  houses <- liftIO $ query c
                           qSearchHouse
                           [name, castle, title, coatArms, motto] :: ActionM [(Int, Text)]

  let json = object
        [ "title"       .= ("House Search" :: Text)
        , "TableHeader" .= ("House"        :: Text)
        , "endpoint"    .= ("houses"       :: Text)
        , "entities"    .= map (jsonifyPairs "Id" "Name") houses
        ]

  --Web.Scotty.json json
  template <- liftAndCatchIO $ currTemplate "templates/searchResults.mustache"
  html $ renderMustache template json


qSearchHouse :: Query
qSearchHouse = " select HouseId            \
               \      , Name               \
               \ from HOUSE                \
               \ where Name       like ?   \
               \    || Castle     like ?   \
               \    || Title      like ?   \
               \    || CoatOfArms like ?   \
               \    || Motto      like ? ; "

factionAction :: ActionM ()
factionAction = do
  c <- liftIO $ connect connectInfo
  blacks <- liftIO $ query c qFaction ["Blacks" :: Text] :: ActionM [(Int, Text)]
  greens <- liftIO $ query c qFaction ["Greens" :: Text] :: ActionM [(Int, Text)]

  let json = object
        [ "title"  .= ("Factions" :: Text)
        , "blacks" .= map (jsonifyPairs "HouseId" "Name") blacks
        , "greens" .= map (jsonifyPairs "HouseId" "Name") greens
        ]

  --Web.Scotty.json json
  template <- liftAndCatchIO $ currTemplate "templates/factions.mustache"
  html $ renderMustache template json

qFaction :: Query
qFaction = " select HouseId       \
           \      , Name          \
           \ from   HOUSE         \
           \ where  Faction = ? ; "

characterInputAction :: ActionM ()
characterInputAction = do
  template <- liftAndCatchIO $ currTemplate "templates/character-input.mustache"
  let json = object
        [ "title" .= ("CHARACTER input" :: Text)]
  html $ renderMustache template json

postCharacterAction :: ActionM ()
postCharacterAction = do
  ps <- params
  let values = p2 ps l2
  -- liftIO $ sequence_ $ map TIO.putStrLn $ map (maybe "Nothing" id) values -- devel output
  c <- liftIO $ connect connectInfo
  liftIO $ execute c qInsertCharacter values
  characterInputAction

-- order decides order of parameters given to execute
-- which must match the parameterized query
l2 :: [Text]
l2 = ["Name", "Title", "Alias", "Culture", "Religion", "BirthDate", "DeathDate"]

p2 :: [Web.Scotty.Param] -> [Text] -> [Maybe Text]
p2 params = fmap (g2 params)

-- this one must transform "" into Nothing
-- so that MySQL.Simple transforms into NULLs
g2 :: [Web.Scotty.Param] -> Text -> Maybe Text
g2 []    _                  = Nothing
g2 ((key, value): xs) field
  | key == field = case value of
                     "" -> Nothing
                     _  -> Just value
  | otherwise    = g2 xs field


qInsertCharacter :: Query
qInsertCharacter = " insert into CHARACTER_ ( Name      \
                   \                        , Title     \
                   \                        , Alias     \
                   \                        , Culture   \
                   \                        , Religion  \
                   \                        , BirthDate \
                   \                        , DeathDate \
                   \                        )           \
                   \ values (?, ?, ?, ?, ?, ?, ?) ;     "

characterInputRelationsAction :: ActionM ()
characterInputRelationsAction = do
  c <- liftIO $ connect connectInfo
  characters <- liftIO $ query_ c qCharactersIdName :: ActionM [(Int, Text)]
  houses     <- liftIO $ query_ c qHousesId         :: ActionM [(Int, Text)]
  let json = object
        [ "title" .= ("CHARACTER relations input" :: Text)

        , "characters" .= map ( \(id, name) -> object [ "CharId" .= id
                                                      , "Name"   .= name
                                                      ])
                              characters
        , "houses"    .= map  ( \(id, name) -> object [ "HouseId" .= id
                                                      , "Name"    .= name
                                                      ])
                              houses
        ]
  --Web.Scotty.json json
  template <- liftAndCatchIO $ currTemplate "templates/character-inputRelations.mustache"
  html $ renderMustache template json

-- could have used qCharacters
qCharactersIdName :: Query
qCharactersIdName = "select CharId     \
                    \     , Name       \
                    \from CHARACTER_ ; "

postCharacterRelationsAction :: ActionM ()
postCharacterRelationsAction = do
  charId <- param "CharId" :: ActionM Int
  ps <- params
  let listHouses   = buildTuples ps "loyal_to" charId
  let listChildren = buildTuples ps "parentOf" charId
  let listSpouses  = buildTuples ps "spouseOf" charId

  -- liftIO $ print charId
  -- liftIO $ print $ listHouses
  -- liftIO $ print $ listChildren
  -- liftIO $ print $ listSpouses

  c <- liftIO $ connect connectInfo

  liftIO $ executeMany c
                       "insert into LOYAL_TO values (?, ?);"
                       listHouses
  case listChildren of
    [] -> pure ()
    _  -> liftIO $ executeMany c
                               "insert into PARENT_OF values (?, ?);"
                               listChildren
          >> pure ()

  case listSpouses of
    [] -> pure ()
    _  -> liftIO $ executeMany c
                               "insert into SPOUSE_OF values (?, ?);"
                               listSpouses
          >> pure ()

  characterInputRelationsAction


buildTuples :: [(Text, Text)] -> Text -> Int -> [(Int, Text)]
buildTuples params key id = transform l
  where
    l = filter (\(k, v) -> k == key) params

    transform :: [(Text, Text)] -> [(Int, Text)]
    transform [] = []
    transform ((k,v) :xs) = case v of
                              "" -> []
                              _  -> (id, v) : transform xs

charactersAction :: ActionM ()
charactersAction = do
  c <- liftIO $ connect connectInfo
  characters <- liftIO $ query_ c qCharacters :: ActionM [Character]
  let json = object
        [ "title"      .= ("Characters" :: Text)
        , "characters" .= characters
        ]
  --Web.Scotty.json json
  template <- liftAndCatchIO $ currTemplate "templates/characters.mustache"
  html $ renderMustache template json

qCharacters :: Query
qCharacters = " select *          \
              \ from CHARACTER_ ; "

characterAction :: ActionM ()
characterAction = do
  id <- param "id" :: ActionM Int
  c  <- liftIO $ connect connectInfo
  characters <- liftIO $ query c qCharacter [id] :: ActionM [Character]

  -- liftIO $ print characters -- devel output

  case characters of
    [] -> status imATeapot418 >> html "BOOM" -- no character with such id
    _  -> do
      let character = head characters
      -- calculate character's age
      let aged = case ((birthDate character), (deathDate character)) of
                   (Nothing, _       ) -> Nothing
                   (_      , Nothing ) -> Nothing
                   (Just bd, Just 0  ) -> Just (129 - bd)
                   (Just bd, Just dd ) -> Just (dd - bd)

      -- liftIO $ print aged -- devel output

      let haveDeath = case (deathDate character) of
                     Nothing -> False
                     Just 0  -> False
                     Just dd -> True

      parents  <- liftIO $ query c qParents      [id] :: ActionM [(Int, Text)]
      children <- liftIO $ query c qChildren     [id] :: ActionM [(Int, Text)]
      spouses  <- liftIO $ query c qSpouses      [id] :: ActionM [(Int, Text)]
      houses   <- liftIO $ query c qHouseChar    [id] :: ActionM [(Int, Text)]
      actors   <- liftIO $ query c qPlayedBy     [id] :: ActionM [(Int, Text)]
      episodes <- liftIO $ query c qCharEpisodes [id] :: ActionM [(Int, Text)]

      let json = object
            [ "title"       .= (nameChar character)

            , "character"   .= character
            , "aged"        .= aged
            , "haveDeath"   .= haveDeath

            , "hasParents" .= (case length parents of 0 -> False; _ -> True )
            , "parents"    .= map (jsonifyPairs "CharId"  "Name") parents

            , "hasChildren" .= (case length children of 0 -> False; _ -> True )
            , "children"    .= map (jsonifyPairs "CharId"  "Name") children

            , "hasSpouses"  .= (case length spouses of 0 -> False; _ -> True )
            , "spouses"     .= map (jsonifyPairs "CharId"  "Name") spouses

            , "houses"      .= map (jsonifyPairs "HouseId" "Name") houses

            , "hasActors"   .= (case length actors of 0 -> False; _ -> True )
            , "actors"      .= map (jsonifyPairs "ActorId" "Name") actors

            , "hasEpisodes" .= (case length episodes of 0 -> False; _ -> True )
            , "episodes"    .= map (jsonifyPairs "EpisodeNumber" "EpisodeName") episodes
            ]

      --Web.Scotty.json json
      template <- liftAndCatchIO $ currTemplate "templates/character.mustache"
      html $ renderMustache template json

jsonifyPairs :: Data.Aeson.Key -> Data.Aeson.Key -> (Int, Text) -> Value
jsonifyPairs field1 field2 (id, name) =
  object
    [ field1 .= id
    , field2 .= name
    ]

qCharacter :: Query
qCharacter = " select *           \
             \ from CHARACTER_    \
             \ where CharId = ? ; "

qParents :: Query
qParents = " select C1.CharId                            \
          \      , C1.Name                              \
          \ from CHARACTER_ C1                          \
          \ join PARENT_OF P on (C1.CharId = P.Parent)  \
          \ join CHARACTER_ C2 on (P.Child = C2.CharId) \
          \ where C2.CharId = ? ;                       "

qChildren :: Query
qChildren = " select C.CharId                           \
            \      , C.Name                             \
            \ from PARENT_OF P                          \
            \ join CHARACTER_ C on (P.Child = C.CharId) \
            \ where P.Parent = ? ;                      "

qSpouses :: Query
qSpouses = " select C.CharId                               \
           \      , C.Name                                 \
           \ from SPOUSE_OF S                              \
           \ join CHARACTER_ C on (S.SpousedTo = C.CharId) \
           \ where S.SpousedFrom = ? ;                     "

qHouseChar :: Query
qHouseChar = " select H.HouseId     \
             \      , H.Name        \
             \ from LOYAL_TO L      \
             \ natural join HOUSE H \
             \ where L.CharId = ? ; "

qPlayedBy :: Query
qPlayedBy = " select ActorId    \
            \      , Name       \
            \ from ACTOR        \
            \ where Plays = ? ; "

qCharEpisodes :: Query
qCharEpisodes = " select E.EpisodeNumber                           \
                \      , E.EpisodeTitle                            \
                \ from CHARACTER_ C                                \
                \ join ACTOR A    on (C.CharId  = A.Plays)         \
                \ join ACTS_IN AI on (A.ActorId = AI.HasActor)     \
                \ join EPISODE E  on (AI.ActsIn = E.EpisodeNumber) \
                \ where C.CharId = ?                               \
                \ order by E.EpisodeNumber ;                       "

characterSearchAction :: ActionM ()
characterSearchAction = do
  ps <- params
  let name  = defaultHeadWrapped $ parseMultiValued "name"  ps
  let title = defaultHeadWrapped $ parseMultiValued "title" ps
  let alias = defaultHeadWrapped $ parseMultiValued "alias" ps

  c  <- liftIO $ connect connectInfo
  characters <- liftIO $ query c
                               qSearchChars
                               [name, title, alias] :: ActionM [(Int, Text)]

  let json = object
        [ "title"       .= ("Character Search" :: Text)
        , "TableHeader" .= ("Character"        :: Text)
        , "endpoint"    .= ("characters"       :: Text)
        , "entities"    .= map (jsonifyPairs "Id" "Name") characters
        ]

  --Web.Scotty.json json
  template <- liftAndCatchIO $ currTemplate "templates/searchResults.mustache"
  html $ renderMustache template json

qSearchChars :: Query
qSearchChars = " select CharId        \
               \      , Name          \
               \ from CHARACTER_      \
               \ where Name  like ?   \
               \    || Title like ?   \
               \    || Alias like ? ; "

actorsAction :: ActionM ()
actorsAction = do
  c <- liftIO $ connect connectInfo
  actors <- liftIO $ query_ c qActors :: ActionM [(Int, Text)]
  let json = object
        [ "title"  .= ("Actors" :: Text)
        , "actors" .= map ( \(id, name) -> object [ "ActorId" .= id
                                                  , "Name"   .= name
                                                  ])
                                actors
        ]
  --Web.Scotty.json json
  template <- liftAndCatchIO $ currTemplate "templates/actors.mustache"
  html $ renderMustache template json

qActors :: Query
qActors = " select ActorId \
          \      , Name    \
          \ from ACTOR ;   "

actorAction :: ActionM ()
actorAction = do
  id <- param "id" :: ActionM Int
  c  <- liftIO $ connect connectInfo
  actors <- liftIO $ query c qActor [id] :: ActionM [Actor]

  case actors of
    [] -> status imATeapot418 >> html "BOOM" -- no actor with such id
    _  -> do
      actor_ <- liftIO $ completeActor c $ head actors
      -- calculate actor's age
      aged <- case (birthDateActor actor_) of
                Nothing   ->  pure Nothing
                Just date -> do
                  curTime  <- liftIO getCurrentTime
                  let today = utctDay curTime
                  pure $ Just $ diffDays today date `div` 365
      let actor = actor_ { age = aged }

      --liftIO $ print actor

      episodes <- liftIO $ query c qActorEpisodes [id] :: ActionM [(Int, Text)]
      let json = object
            [ "title"     .= (nameActor actor)
            , "actor"     .= actor
            , "hasAwards" .= (case length (actorAwards actor) of 0 -> False; _ -> True )
            , "episodes"  .= map (\(id, title) -> object [ "EpisodeNumber" .= id
                                                         , "EpisodeTitle"  .= title
                                                         ])
                                 episodes
            ]
      --Web.Scotty.json json
      template <- liftAndCatchIO $ currTemplate "templates/actor.mustache"
      html $ renderMustache template json


completeActor :: Connection -> Actor -> IO Actor
completeActor c a = do
  awards <- query c qActorAwards [actorId a] :: IO [ActorAward]
  pure $ a { actorAwards = awards }

qActor :: Query
qActor = " select A.ActorId            \
         \      , A.Name               \
         \      , A.Gender             \
         \      , A.FilmographyCredits \
         \      , A.BirthDate          \
         \      , A.BirthCountry       \
         \      , A.Plays              \
         \      , C.Name               \
         \ from ACTOR A                \
         \ join CHARACTER_ C           \
         \ on (A.Plays = C.CharId)     \
         \ where A.ActorId = ? ;       "

qActorAwards :: Query
qActorAwards = " select AwardName    \
               \      , AwardEntity  \
               \ from ACTOR_AWARD    \
               \ where ActorId = ? ; "


qActorEpisodes :: Query
qActorEpisodes = " select E.EpisodeNumber                \
                 \      , E.EpisodeTitle                 \
                 \ from ACTS_IN AI                       \
                 \ join EPISODE E                        \
                 \      on (AI.ActsIn = E.EpisodeNumber) \
                 \ where AI.HasActor = ?                 \
                 \ order by E.EpisodeNumber ;            "

actorSearchAction :: ActionM ()
actorSearchAction = do
  ps <- params
  let name         = defaultHeadWrapped $ parseMultiValued "name"         ps
  let birthCountry = defaultHeadWrapped $ parseMultiValued "birthcountry" ps
  let awardName    = defaultHeadWrapped $ parseMultiValued "awardname"    ps
  let awardEntity  = defaultHeadWrapped $ parseMultiValued "awardentity"  ps

  c  <- liftIO $ connect connectInfo
  actors <- liftIO $ query c
                           qSearchActors
                           [name, birthCountry, awardName, awardEntity] :: ActionM [(Int, Text)]

  let json = object
        [ "title"       .= ("Actor Search" :: Text)
        , "TableHeader" .= ("Actor"        :: Text)
        , "endpoint"    .= ("actors"       :: Text)
        , "entities"    .= map (jsonifyPairs "Id" "Name") actors
        ]

  --Web.Scotty.json json
  template <- liftAndCatchIO $ currTemplate "templates/searchResults.mustache"
  html $ renderMustache template json

defaultHeadWrapped :: [Text] -> Text
defaultHeadWrapped []     = ""
defaultHeadWrapped (x:xs) = case x of
                              "" -> ""
                              _  -> Data.Text.Lazy.concat ["%", x, "%"]

qSearchActors :: Query
qSearchActors = " select distinct A.ActorId                            \
                \               , A.Name                               \
                \ from ACTOR A                                         \
                \ left join ACTOR_AWARD AD on (A.ActorId = AD.ActorId) \
                \ where Name         like ?                            \
                \    || BirthCountry like ?                            \
                \    || AwardName    like ?                            \
                \    || AwardEntity  like ? ;                          "

episodesAction :: ActionM ()
episodesAction = do
  c <- liftIO $ connect connectInfo
  episodes <- liftIO $ query_ c qEpisodes :: ActionM [(Int, Text)]
  let json = object
        [ "title"    .= ("Episodes" :: Text)
        , "episodes" .= map ( \(id, name) -> object [ "EpisodeNumber" .= id
                                                    , "EpisodeTitle"  .= name
                                                    ])
                                episodes
        ]

  --Web.Scotty.json json
  template <- liftAndCatchIO $ currTemplate "templates/episodes.mustache"
  html $ renderMustache template json



qEpisodes :: Query
qEpisodes = " select EpisodeNumber     \
            \      , EpisodeTitle      \
            \ from EPISODE             \
            \ order by EpisodeNumber ; "

episodeAction :: ActionM ()
episodeAction = do
  id <- param "id" :: ActionM Int
  c  <- liftIO $ connect connectInfo
  episodes <- liftIO $ query c qEpisode [id] :: ActionM [Episode]

  -- liftIO $ print episodes -- devel output

  case episodes of
    [] -> status imATeapot418 >> html "BOOM" -- no episode with such id
    _  -> do
      let episode = head episodes
      characters <- liftIO $ query c
                                   qEpisodeChars
                                   [episodeNumber episode]:: ActionM [(Int, Text)]
      let json = object
            [ "title"      .= (episodeTitle episode)
            , "episode"    .= episode
            , "characters" .= map (\(id, name) -> object [ "CharId" .= id
                                                         , "Name"   .= name
                                                         ])
                                  characters
            ]
      --Web.Scotty.json json
      template <- liftAndCatchIO $ currTemplate "templates/episode.mustache"
      html $ renderMustache template json



qEpisode :: Query
qEpisode = " select *                 \
           \ from EPISODE             \
           \ where EpisodeNumber = ? ;"

qEpisodeChars :: Query
qEpisodeChars = " select C.CharId                              \
                \      , C.Name                                \
                \ from CHARACTER_ C                            \
                \ join ACTOR A    on (C.CharId = A.Plays)      \
                \ join ACTS_IN AI on (A.ActorId = AI.HasActor) \
                \ where AI.ActsIn = ? ;                        "

episodeSearchAction :: ActionM ()
episodeSearchAction = do
  ps <- params
  let director     = defaultHeadWrapped $ parseMultiValued "director"     ps
  let screenwriter = defaultHeadWrapped $ parseMultiValued "screenwriter" ps

  c  <- liftIO $ connect connectInfo
  episodes <- liftIO $ query c
                           qSearchEpisodes
                           [director, screenwriter] :: ActionM [(Int, Text)]

  let json = object
        [ "title"       .= ("Episode Search" :: Text)
        , "TableHeader" .= ("Episode"        :: Text)
        , "endpoint"    .= ("episodes"       :: Text)
        , "entities"    .= map (jsonifyPairs "Id" "Name") episodes
        ]

  --Web.Scotty.json json
  template <- liftAndCatchIO $ currTemplate "templates/searchResults.mustache"
  html $ renderMustache template json

qSearchEpisodes :: Query
qSearchEpisodes = " select EpisodeNumber        \
                  \      , EpisodeTitle         \
                  \ from EPISODE                \
                  \ where Director like     ?   \
                  \    || Screenwriter like ? ; "
