{- stack script
 --resolver lts-20.3
 --package "scotty mysql-simple wai wai-middleware-static wai-extra text microstache aeson http-types time"
-}

{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty

-- local modules
import Middlewares (staticMiddleware, logStdoutDev)
import Actions
  ( rootAction
  , notFoundAction
  , searchAction
  , regionInputAction
  , postRegionAction
  , regionsAction
  , regionIdAction
  , regionSearchAction
  , houseInputAction
  , postHouseAction
  , housesAction
  , houseIdAction
  , houseSearchAction
  , factionAction
  , characterInputAction
  , postCharacterAction
  , characterInputRelationsAction
  , postCharacterRelationsAction
  , charactersAction
  , characterAction
  , characterSearchAction
  , actorsAction
  , actorAction
  , actorSearchAction
  , episodesAction
  , episodeAction
  , episodeSearchAction
  )

main :: IO ()
main = scotty 3000 app

app :: ScottyM ()
app =
 -- middleware :: Middleware -> ScottyM ()
    middleware staticMiddleware
 >> middleware logStdoutDev

 -- defaultHandler :: (Text -> ActionM ()) -> ScottyM ()
 >> defaultHandler (\_ -> text "You broke me...") -- TODO: build better response

 -- get :: RoutPattern -> ActionM () -> ScottyM ()
 >> get  "/"                           rootAction
 >> get  "/search"                     searchAction
 >> get  "/regions"                    regionsAction
 >> get  "/regions/input"              regionInputAction
 >> post "/regions/input"              postRegionAction
 >> get  "/regions/:id"                regionIdAction
 >> get  "/regions/search"             regionSearchAction
 >> get  "/houses/input"               houseInputAction
 >> post "/houses/input"               postHouseAction
 >> get  "/houses"                     housesAction
 >> get  "/houses/:id"                 houseIdAction
 >> get  "/houses/search"              houseSearchAction
 >> get  "/factions"                   factionAction
 >> get  "/characters/input"           characterInputAction
 >> post "/characters/input"           postCharacterAction
 >> get  "/characters/input-relations" characterInputRelationsAction
 >> post "/characters/input-relations" postCharacterRelationsAction
 >> get  "/characters"                 charactersAction
 >> get  "/characters/:id"             characterAction
 >> get  "/characters/search"          characterSearchAction
 >> get  "/actors"                     actorsAction
 >> get  "/actors/:id"                 actorAction
 >> get  "/actors/search"              actorSearchAction
 >> get  "/episodes"                   episodesAction
 >> get  "/episodes/:id"               episodeAction
 >> get  "/episodes/search"            episodeSearchAction

 >> notFound                           notFoundAction
