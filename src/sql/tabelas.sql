drop   database if exists     guest;
create database if not exists guest;
use guest;


create table REGION (
RegionId         int         auto_increment,
Name             varchar(16) not null unique,
PopulationName   varchar(16) not null,
BastardsSurname  varchar(16) not null,
RuledBy          int         not null,

primary key (RegionId)
);


create table PLACE_OF_NOTE (
RegionId         int,
PlaceOfNote      varchar(32),

primary key (RegionId, PlaceOfNote),
foreign key (RegionId) references REGION(RegionId)
                       on delete cascade
                       on update cascade
);


create table HOUSE (
HouseId          int                                auto_increment,
Name             varchar(32)                        not null unique,
Faction          enum('Blacks','Greens', 'Neutral'),
Castle           varchar(32)                        not null unique,
Title            varchar(64)                        not null unique,
CoatOfArms       varchar(256)                       not null unique,
Motto            varchar(32)                        unique,
OverlordedBy     int                                not null,

primary key (HouseId),
foreign key (overlordedBy) references HOUSE(HouseId)
                           on update cascade
                           on delete cascade
);


create table CHARACTER_ (                        -- CHARACTER is a reserved word
CharId           int            auto_increment,
Name             varchar(32)    not null unique, -- not unique in the story but here happen to be
Title            varchar(64),
Alias            varchar(32),
BirthDate        int,
DeathDate        int,                            -- null is no date, 0 is ALIVE

primary key (CharId)
);


create table LOYAL_TO (
CharId           int,
HouseId          int,

primary key (CharId, HouseId),
foreign key (CharId)  references CHARACTER_(CharId)
                      on update cascade
                      on delete cascade,
foreign key (HouseId) references HOUSE(HouseId)
                      on update cascade
                      on delete cascade
);

create table PARENT_OF (
Parent          int,
Child           int,

primary key (Parent, Child),
foreign key (Parent) references CHARACTER_(CharId)
                     on update cascade
                     on delete cascade,
foreign key (Child)  references CHARACTER_(CharId)
                     on update cascade
                     on delete cascade
);

create table SPOUSE_OF (
SpousedFrom      int,
SpousedTo        int,

primary key (SpousedFrom, SpousedTo),
foreign key (SpousedFrom) references CHARACTER_(CharId)
                          on update cascade
                          on delete cascade,
foreign key (SpousedTo)   references CHARACTER_(CharId)
                          on update cascade
                          on delete cascade
);


create table ACTOR (
ActorId            int            auto_increment,
Name               varchar(32)    unique not null,
Gender             enum('M','F')  not null,
FilmographyCredits int            not null,
BirthDate          date,
BirthCountry       varchar(16),
Plays              int            not null,

primary key (ActorId),
foreign key (Plays) references CHARACTER_(CharId)
                    on update cascade
                    on delete cascade
);

create table ACTOR_AWARD (
ActorId          int,
AwardName        varchar(128),
AwardEntity      varchar(64),

primary key (ActorId, AwardName, AwardEntity),
foreign key (ActorId) references ACTOR(ActorId)
                     on update cascade
                     on delete cascade
);


create table EPISODE (
EpisodeNumber    int,
EpisodeTitle     varchar(32)    unique not null,
Director         varchar(32)    not null,
Screenwriter     varchar(32)    not null,
ReleaseDate      date           not null,
UserViews        int            not null,

primary key (EpisodeNumber)
);


create table ACTS_IN (
ActsIn           int,
HasActor         int,

primary key (ActsIn, HasActor),
foreign key (ActsIn)   references EPISODE(EpisodeNumber)
                       on update cascade
                       on delete cascade,
foreign key (HasActor) references ACTOR(ActorId)
                       on update cascade
                       on delete cascade
);
