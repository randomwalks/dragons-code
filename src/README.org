* Sumário

Aplicação escrita em Haskell.

* Instalação de dependências

Haskell. Há várias formas de o obter listo duas possibilidades:
- [[https://www.haskell.org/ghcup/][GHCup]]
- [[https://docs.haskellstack.org/en/stable/][stack]]

MariaDB / MySQL
- Escolher e instalar um deles.

* Configuração da BD

Editar, se necessário, connectInfo no ficheiro Actions.hs, os campos são auto-explicativos.
Carregar sql/tabelas.sql e posterioremente sql/dados.sql para MySQL/MariaDB.

* Execução

A primeira vez que correr será morosa pois o stack irá fazer o download das dependências e terá de as compilar, tudo automático.

#+begin_src shell
  stack Main.hs
  # Setting phasers to stun... (port 3000) (ctrl-c to quit)
#+end_src

* Acesso

[[http://localhost:3000/]]
