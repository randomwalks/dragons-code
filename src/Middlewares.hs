{-# LANGUAGE OverloadedStrings #-}

module Middlewares
  ( staticMiddleware
  , logStdoutDev
  ) where

import Network.Wai (Middleware)
import Network.Wai.Middleware.Static
import Network.Wai.Middleware.RequestLogger (logStdoutDev)

staticMiddleware :: Middleware
staticMiddleware =
  staticPolicy $ -- :: Policy -> Middleware
    -- (>->) :: Policy -> Policy -> Policy
    noDots >-> isNotAbsolute >-> (hasPrefix "static")
