{-# LANGUAGE OverloadedStrings #-}

module Types
  ( Region     (..)
  , House      (..)
  , Character  (..)
  , Actor      (..)
  , ActorAward (..)
  , Episode    (..)
  ) where

import Data.Aeson hiding (Result) -- to use Result from MySQL.Simple.Result without qualification
import Data.Text.Lazy (Text)

import Database.MySQL.Simple.QueryResults
import Database.MySQL.Simple.Result (convert, Result)

import Data.Time.Calendar (Day)

data Region = Region
  { regionId        :: Int
  , nameRegion      :: Text
  , populationName  :: Text
  , bastardsSurname :: Text
  , placeOfNote     :: [Text]
  , ruledBy         :: Int
  , ruledByName     :: Text
  } deriving Show

instance ToJSON Region where
--toJSON :: Region -> Value
  toJSON r = object
    [ "RegionId"        .= (regionId        r)
    , "Name"            .= (nameRegion      r)
    , "PopulationName"  .= (populationName  r)
    , "BastardsSurname" .= (bastardsSurname r)
    , "PlaceOfNote"     .= map (\place -> object ["place" .= place]) (placeOfNote     r) -- [place: "somewhere", ...]
    , "RuledBy"         .= (ruledBy         r)
    , "RuledByName"     .= (ruledByName     r)
    ]

instance QueryResults Region where
--convertResults : [Field] -> [Maybe ByteString] -> Region
  convertResults [fa, fb, fc, fd, fe, ff] [va, vb, vc, vd, ve, vf] =
    Region a b c d [] e f

    where !a = convert fa va
          !b = convert fb vb
          !c = convert fc vc
          !d = convert fd vd
          !e = convert fe ve
          !f = convert ff vf

  convertResults fs vs = convertError fs vs 6

data Faction
  = Blacks
  | Greens
  | Neutral

instance ToJSON Faction where
--toJSON :: Faction -> Value
  toJSON Blacks  = toJSON ("Blacks"  :: Text)
  toJSON Greens  = toJSON ("Greens"  :: Text)
  toJSON Neutral = toJSON ("Neutral" :: Text)

instance Result Faction where
--convert :: Field -> Maybe ByteString -> Faction
  convert _ (Just "Blacks")  = Blacks
  convert _ (Just "Greens")  = Greens
  convert _ (Just "Neutral") = Neutral
  convert _ Nothing          = Neutral
data House = House
  { houseId          :: Int
  , nameHouse        :: Text
  , faction          :: Maybe Faction
  , castle           :: Text
  , title            :: Text
  , coatOfArms       :: Text
  , motto            :: Maybe Text
  , overlordedBy     :: Int
  , overlordedByName :: Text -- queries will include them so the name can be presented to the user
  , houseRegionId    :: Maybe Int
  , houseRegionName  :: Maybe Text -- <Maybe> because in /houses we do not wish to know each house region, will be obtained by separete recursive query
  }

instance ToJSON House where
--toJSON :: House -> Value
  toJSON h = object
    [ "HouseId"          .= (houseId          h)
    , "Name"             .= (nameHouse        h)
    , "Faction"          .= (faction          h)
    , "Castle"           .= (castle           h)
    , "Title"            .= (title            h)
    , "CoatOfArms"       .= (coatOfArms       h)
    , "Motto"            .= (motto            h)
    , "OverlordedBy"     .= (overlordedBy     h)
    , "OverlordedByName" .= (overlordedByName h)
    , "HouseRegionId"    .= (houseRegionId    h)
    , "HouseRegionName"  .= (houseRegionName  h)
    ]

instance QueryResults House where
  --convertResults : [Field] -> [Maybe ByteString] -> House
  convertResults [fa, fb, fc, fd, fe, ff, fg, fh, fi] [va, vb, vc, vd, ve, vf, vg, vh, vi] =
    House a b c d e f g h i Nothing Nothing

    where !a = convert fa va
          !b = convert fb vb
          !c = convert fc vc
          !d = convert fd vd
          !e = convert fe ve
          !f = convert ff vf
          !g = convert fg vg
          !h = convert fh vh
          !i = convert fi vi

  convertResults fs vs = convertError fs vs 9

data Character = Character
  { charId :: Int
  , nameChar  :: Text
  , titleChar :: Maybe Text
  , alias     :: Maybe Text
  , birthDate :: Maybe Int
  , deathDate :: Maybe Int
  } deriving Show

instance ToJSON Character where
--toJSON :: Character -> Value
  toJSON c = object
    [ "CharId"    .= (charId    c)
    , "Name"      .= (nameChar  c)
    , "Title"     .= (titleChar c)
    , "Alias"     .= (alias     c)
    , "BirthDate" .= (birthDate c)
    , "DeathDate" .= (deathDate c) -- TODO: should the 0 to mean Alive be translated?
    ]

instance QueryResults Character where
--convertResults : [Field] -> [Maybe ByteString] -> Character
  convertResults [fa, fb, fc, fd, fe, ff] [va, vb, vc, vd, ve, vf] =
    Character a b c d e f

    where !a = convert fa va
          !b = convert fb vb
          !c = convert fc vc
          !d = convert fd vd
          !e = convert fe ve
          !f = convert ff vf

  convertResults fs vs = convertError fs vs 6

data Gender = Female | Male
  deriving Show

instance ToJSON Gender where
--toJSON :: Gender -> Value
  toJSON Female = toJSON ("Female" :: Text)
  toJSON Male   = toJSON ("Male" :: Text)

instance Result Gender where
--convert :: Field -> Maybe ByteString -> Gender
  convert _ Nothing    = Female
  convert _ (Just "F") = Female
  convert _ (Just "M") = Male

data ActorAward = ActorAward
  { awardName   :: Text
  , awardEntity :: Text
  } deriving Show

instance ToJSON ActorAward where
--toJSON :: ActorAward -> Value
  toJSON a = object
    [ "AwardName"   .= (awardName a)
    , "AwardEntity" .= (awardEntity a)
    ]

instance QueryResults ActorAward where
 --convertResults : [Field] -> [Maybe ByteString] -> ActorAward
  convertResults [fa, fb] [va, vb] =
    ActorAward a b

    where !a = convert fa va
          !b = convert fb vb

  convertResults fs vs = convertError fs vs 2

data Actor = Actor
  { actorId            :: Int
  , nameActor          :: Text
  , gender             :: Gender
  , filmographyCredits :: Int
  , birthDateActor     :: Maybe Day
  , age                :: Maybe Integer -- diffDays returns Integer
  , birthCountry       :: Maybe Text
  , actorAwards        :: [ActorAward]
  , plays              :: Int
  , playsName          :: Text
  } deriving Show

instance ToJSON Actor where
--toJSON :: Actor -> Value
  toJSON a = object
    [ "ActorId"            .= (actorId            a)
    , "Name"               .= (nameActor          a)
    , "Gender"             .= (gender             a)
    , "FilmographyCredits" .= (filmographyCredits a)
    , "BirthDate"          .= (birthDateActor     a)
    , "Age"                .= (age                a)
    , "BirthCountry"       .= (birthCountry       a)
    , "ActorAwards"        .= (actorAwards        a)
    , "Plays"              .= (plays              a)
    , "PlaysName"          .= (playsName          a)
    ]

instance QueryResults Actor where
--convertResults : [Field] -> [Maybe ByteString] -> Actor
  convertResults [fa, fb, fc, fd, fe, ff, fg, fh] [va, vb, vc, vd, ve, vf, vg, vh] =
    Actor a b c d e Nothing f [] g h

    where !a = convert fa va
          !b = convert fb vb
          !c = convert fc vc
          !d = convert fd vd
          !e = convert fe ve
          !f = convert ff vf
          !g = convert fg vg
          !h = convert fh vh

  convertResults fs vs = convertError fs vs 8

data Episode = Episode
  { episodeNumber :: Int
  , episodeTitle  :: Text
  , director      :: Text
  , screenwriter  :: Text
  , releaseDate   :: Day
  , userViews     :: Int
  } deriving Show

instance ToJSON Episode where
--toJSON :: Episode -> Value
  toJSON e = object
    [ "EpisodeNumber"  .= (episodeNumber         e)
    , "EpisodeTitle"   .= (episodeTitle          e)
    , "Director"       .= (director              e)
    , "Screenwriter"   .= (screenwriter          e)
    , "ReleaseDate"    .= (releaseDate           e)
    , "UserViews-int"  .= ((userViews e) `div` 100)
    , "UserViews-frac" .= ((userViews e) `mod` 100)
    ]

instance QueryResults Episode where
--convertResults : [Field] -> [Maybe ByteString] -> Episode
  convertResults [fa, fb, fc, fd, fe, ff] [va, vb, vc, vd, ve, vf] =
    Episode a b c d e f

    where !a = convert fa va
          !b = convert fb vb
          !c = convert fc vc
          !d = convert fd vd
          !e = convert fe ve
          !f = convert ff vf

  convertResults fs vs = convertError fs vs 6
